$(document).ready(function(){
  var getUserInfo = {
    url: 'https://randomuser.me/api/',
    dataType: 'json'
  };

  // hacer llamada jQuery
  $.ajax(getUserInfo)
    .done(function(user){
      console.log('Llamada completada', user);
      var usuario = user.results[0];
      console.log('Objeto del usuario', usuario);

      // concatenar nombres
      var nombre = usuario.name.first + ' ' + usuario.name.last;
      $('#cv_nombre').text(nombre);
      $('#cv_foto').attr('src', usuario.picture.large);

      // imprimir correo
      $('#cv_correo').text(usuario.email);

      $('#cv_localizacion').text(usuario.location.city)
    })
    .fail(function(error, error2){
      console.log('Llamada con error', error);
      console.log('Llamada con error2', error2);
    });
});
